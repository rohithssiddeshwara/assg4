FROM ubuntu:22.04

RUN apt update && apt install apache2 -y

# CMD rm /var/www/html/index.html

COPY index.html /var/www/html/index.html

RUN service apache2 start

EXPOSE 81:80



